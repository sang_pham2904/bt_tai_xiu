import React, { useState } from "react";
import "./Game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export const TAI = "TÀI";
export const XIU = "XĨU";

export default function GameTaiXiu() {
  const [xucXacArr, setXucXacArr] = useState([
    { value: 1, img: "./GameImg/1.png" },
    { value: 1, img: "./GameImg/1.png" },
    { value: 1, img: "./GameImg/1.png" },
  ]);
  const [luaChon, setLuaChon] = useState(null);
  const [ketQua, setKetQua] = useState(null);
  const [taiXiu, setTaiXiu] = useState(null);
  const [tongSoBanChoi, setTongSoBanChoi] = useState(0);
  const [soBanThang, setSoBanThang] = useState(0);

  let handleDatCuoc = (value) => {
    setLuaChon(value);
  };

  let handlePlay = () => {
    if (luaChon == null) {
      alert("Vui lòng đặt cược");
    } else {
        let count = 0;
        let myInterval = setInterval(() => {
        let newXucXacArr = xucXacArr.map(() => {
          let numberRandom = Math.floor(Math.random() * 6) + 1;
          return { value: numberRandom, img: `./GameImg/${numberRandom}.png` };
        });
        setXucXacArr(newXucXacArr);
        count += 1;
  

        if (count == 10) {
          clearInterval(myInterval);
          let ketQua = newXucXacArr.reduce((tong, item, index) => {
            return (tong += item.value);
          }, 0);
          setKetQua(`${ketQua} nút`);

          setTaiXiu(ketQua >= 11 ? TAI : XIU);
          setTongSoBanChoi(tongSoBanChoi + 1);

          let cloneSoBanThang = soBanThang;
          if (ketQua >= 11 && luaChon == TAI) {
            cloneSoBanThang += 1;
          }
          if (ketQua < 11 && luaChon == XIU) {
            cloneSoBanThang += 1;
          }
          setSoBanThang(cloneSoBanThang);
        }

          console.log(count);
      }, 100);
    }
  };
  return (
    <div
      className="text-center game-xuc-xac"
      style={{ background: `url(./GameImg/bgGame.png)` }}
    >
      <h1 className="py-3">Game Xúc Xắc Tài Xĩu</h1>
      <XucXac xucXacArr={xucXacArr} handleDatCuoc={handleDatCuoc} />
      <KetQua
        luaChon={luaChon}
        handlePlay={handlePlay}
        ketQua={ketQua}
        taiXiu={taiXiu}
        tongSoBanChoi={tongSoBanChoi}
        soBanThang={soBanThang}
      />
    </div>
  );
}
