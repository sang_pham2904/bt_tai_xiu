import React from 'react'

export default function KetQua({luaChon, handlePlay, ketQua, taiXiu,tongSoBanChoi, soBanThang}) {
  return (
    <div>
        <h2>Bạn chọn: <span className='text-success'>{luaChon}</span></h2>
        <button onClick={handlePlay} className='btn btn-success'>Play Game</button>
        <h2>Kết Quả: <span className='text-danger'>{ketQua}</span> <span className='text-warning'>{taiXiu}</span></h2>
        <h2>Tổng số bàn chơi: <span className='text-primary'>{tongSoBanChoi}</span></h2>
        <h2>Số bàn thắng: <span className='text-success'>{soBanThang}</span></h2>

    </div>
  )
}
