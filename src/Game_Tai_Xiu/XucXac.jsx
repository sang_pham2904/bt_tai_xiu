import React from 'react'
import { TAI } from './GameTaiXiu'
import { XIU } from './GameTaiXiu'


export default function XucXac({xucXacArr, handleDatCuoc}) {
   
    let renderXucXac = () => {
return   xucXacArr.map((item,index) =>{
     return <img key={index} src={item.img} style ={{height : 100, margin : 10}} alt="" />
        })
    }
  return (
    <div>
        <div className='d-flex justify-content-center container'>
            <button onClick={() => handleDatCuoc(TAI)} className='btn btn-secondary text-white p-5'>Tài</button>
            <div>{renderXucXac()}</div>
            <button onClick={() => handleDatCuoc(XIU)} className='btn btn-danger p-5'>Xĩu</button>
        </div>
    </div>
  )
}
